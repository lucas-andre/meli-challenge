PROJECT_ID=$(shell gcloud config get-value core/project)
APP=melli-challenge
all:
	@echo "do-all             - Build all the test components"
	@echo "build              - Build the docker image"
	@echo "redis              - Run redis container"
	@echo "deploy             - Deploy the image to Cloud Run"
	@echo "create-memorystore - Create the Memorystore instance"
	@echo "create-connection  - Create the VPC Access Connector"
	@echo "clean-all          - Delete all resources in GCP created by these tests"

cloud-run-vpc-connector:
	gcloud compute networks vpc-access connectors create cloud-run-vpc-connector \
		--network default \
		--region southamerica-west1 \
		--range 10.8.0.0/28

create-memorystore:
	gcloud redis instances create redis-shortener \
		--display-name redis-shorterner \
		--region southamerica-west1 \
		--tier standard \
		--size 5 \
		--read-replicas-mode read-replicas-enabled \
		--replica-count 2

deploy:
	REDIS_HOST=$(shell gcloud redis instances describe redis-shortener --region southamerica-west1 --format='value(host)'); \
	gcloud run deploy $(APP) \
		--image gcr.io/$(PROJECT_ID)/$(APP) \
		--concurrency 1000 \
		--max-instances 50 \
		--min-instances 3 \
		--cpu 4 \
		--memory 4Gi \
		--platform managed \
		--region southamerica-west1 \
		--vpc-connector cloud-run-vpc-connector \
		--allow-unauthenticated \
		--set-env-vars "REDIS_HOST=$$REDIS_HOST" \
		--set-env-vars "ENVIRONMENT=production"
	@url=$(shell gcloud run services describe melli-challenge --format='value(status.url)' --region southamerica-west1 --platform managed); \
	echo "Target URL = $$url"

build:
	gcloud builds submit --tag gcr.io/$(PROJECT_ID)/$(APP)

do-all: create-memorystore cloud-run-vpc-connector build deploy
	@echo "All done!"

clean-redis-forwarder:
	gcloud compute instances delete redis-forwarder --zone southamerica-west1-a --quiet

clean-redis:
	gcloud redis instances delete redis-shortener --region southamerica-west1 

clean-connection:
	gcloud compute networks vpc-access connectors delete cloud-run-vpc-connector --region southamerica-west1 --quiet

clean-cloud-run:
	gcloud run services delete $(APP) --platform managed --region southamerica-west1 --quiet

clean-image:
	gcloud container images delete gcr.io/$(PROJECT_ID)/$(APP):latest --quiet

clean-all: clean-redis clean-connection clean-cloud-run clean-image

create-local-redis:
	docker-compose -f ./redis/docker-compose-redis-only.yml up -d

test-redirect:
	@url=$(shell gcloud run services describe melli-challenge --format='value(status.url)' --region southamerica-west1 --platform managed); \
	echo "Target URL = $$url"; \
	ab -n 10000 -c 150 -m GET -k $$url/2Y4UeBgV/test

test-shortener:
	@url=$(shell gcloud run services describe melli-challenge --format='value(status.url)' --region southamerica-west1 --platform managed); \
	echo "Target URL = $$url"; \
	ab -n 10000 -c 150 -p post -T application/x-www-form-urlencoded -k $$url/

test-local-shorterner:
	echo "Target URL = 127.0.0.1:8080"; \
	ab -n 100000 -c 500 -p post -T application/x-www-form-urlencoded -k http://127.0.0.1:8080/

test-local-redirect:
	echo "Target URL = 127.0.0.1:8080"; \
	ab -n 100000 -c 500 -m GET -k http://127.0.0.1:8080/test/8bZom1Sw 
