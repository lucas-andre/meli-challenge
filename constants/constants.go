package constants

var (
	// ShortURLCreated is the message for a successful short link creation
	ShortURLCreated = "Short URL created"
	// MissingURLParam is the message for the missing url param
	MissingURLParam = "Missing URL parameter"
	// ShortURLValue is the key for the short url value in the context
	ShortURLValue = "shortURL"
	// Production is the environment variable for production
	Production = "production"
	// HTTPS is the const for https
	HTTPS = "https://"
	// HTTP is the const for http
	HTTP = "http://"
)
