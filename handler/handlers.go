package handler

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/fasthttp/router"
	"github.com/lukas-henry/meli-challenge/constants"
	"github.com/lukas-henry/meli-challenge/shortener"
	"github.com/lukas-henry/meli-challenge/store"

	"github.com/valyala/fasthttp"
)

var (
	strContentType     = []byte("Content-Type")
	strApplicationJSON = []byte("application/json")
	env                = os.Getenv("ENVIRONMENT")
)

type baseResponse struct {
	Success  bool   `json:"success"`
	Message  string `json:"message"`
	ShortURL string `json:"shortURL,omitempty"`
}

// New creates a new router
func New() *router.Router {
	router := router.New()
	router.POST("/", CreateShortLink)
	router.GET("/{shortURL}", handleShortLinkRedirect)
	router.GET("/{shortURL}/test", handleTestShortLinkRedirect)
	router.GET("/{shortURL}/info", handleShortLinkInfo)
	router.DELETE("/{shortURL}", handleShortLinkDelete)
	return router
}

// CreateShortLink is the handler for the POST / route and create a short link
func CreateShortLink(c *fasthttp.RequestCtx) {
	URL := string(c.FormValue("url"))
	if URL == "" {
		c.SetStatusCode(fasthttp.StatusBadRequest)
		c.SetBodyString(constants.MissingURLParam)
	} else {
		shortURL := shortener.GenerateShortLink(URL)
		store.SaveURLMapping(shortURL, URL)

		r := &baseResponse{
			Success:  true,
			Message:  constants.ShortURLCreated,
			ShortURL: getBaseURL(c) + "/" + shortURL,
		}
		responseOK(c, r)
	}
}

func handleShortLinkRedirect(c *fasthttp.RequestCtx) {
	shortURL := c.UserValue(constants.ShortURLValue).(string)
	initialURL := store.RetrieveInitialURL(shortURL)
	if initialURL == "" {
		notFound(c, shortURL)
	} else {
		c.Redirect(initialURL, 302)
	}
}

func handleTestShortLinkRedirect(c *fasthttp.RequestCtx) {
	shortURL := c.UserValue(constants.ShortURLValue).(string)
	initialURL := store.RetrieveInitialURL(shortURL)
	if initialURL == "" {
		notFound(c, shortURL)
	} else {
		c.Response.SetStatusCode(fasthttp.StatusOK)
		c.Response.SetBodyString(initialURL)
	}
}

func handleShortLinkInfo(c *fasthttp.RequestCtx) {
	shortURL := c.UserValue(constants.ShortURLValue).(string)
	URLInfo := store.RetrieveURLInfo(shortURL)
	if URLInfo.OriginalURL == "" {
		notFound(c, shortURL)
	} else {
		responseOK(c, URLInfo)
	}

}

func handleShortLinkDelete(c *fasthttp.RequestCtx) {
	shortURL := c.UserValue(constants.ShortURLValue).(string)
	URLInfo := store.RetrieveURLInfo(shortURL)
	if URLInfo.OriginalURL == "" {
		notFound(c, shortURL)
	} else {
		store.DeleteURL(shortURL)
		c.Response.SetStatusCode(fasthttp.StatusNoContent)
	}
}

func notFound(c *fasthttp.RequestCtx, inputURL string) {
	c.Response.SetStatusCode(fasthttp.StatusNotFound)
	c.Response.Header.SetCanonical(strContentType, strApplicationJSON)
	baseResponse := &baseResponse{
		Success: false,
		Message: fmt.Sprintf("URL %s not found", inputURL),
	}
	if err := json.NewEncoder(c).Encode(baseResponse); err != nil {
		c.Error(err.Error(), fasthttp.StatusInternalServerError)
	}
}

func responseOK(c *fasthttp.RequestCtx, br interface{}) {
	c.Response.SetStatusCode(fasthttp.StatusOK)
	c.Response.Header.SetCanonical(strContentType, strApplicationJSON)
	if err := json.NewEncoder(c).Encode(br); err != nil {
		c.Error(err.Error(), fasthttp.StatusInternalServerError)
	}
}

func getBaseURL(c *fasthttp.RequestCtx) string {
	protocol := constants.HTTP
	if env == constants.Production {
		protocol = constants.HTTPS
	}
	return protocol + string(c.Request.Header.Host())
}
