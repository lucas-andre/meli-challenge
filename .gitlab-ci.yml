include:
  - local: .gitlab/templates.yml

stages:
  - Lint
  - Test
  - Cloud resources setup
  - Build
  - Deploy
  - Benchmark
  - Cleanup

variables:
  GCLOUD_PROJECT_ID: inker-prod
  GCLOUD_COMPUTE_ZONE: southamerica-west1-a
  GCLOUD_COMPUTE_REGION: southamerica-west1
  GCLOUD_REDIS_INSTANCE: redis-shortener
  GCLOUD_CLOUD_RUN_VPC_CONNECTOR: cloud-run-vpc-connector

Lint:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:golangci-lint-alpine
  stage: Lint
  script:
    - export GO111MODULE=off
    # Use default .golangci.yml file from the image if one is not present in the project root.
    - '[ -e .golangci.yml ] || cp /golangci/.golangci.yml .'
    - golangci-lint run --issues-exit-code 0 --out-format code-climate | tee gl-code-quality-report.json | jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    paths:
      - gl-code-quality-report.json

Build & Test:
  needs: [Lint]
  image: golang:latest
  stage: Test
  services:
    - redis:latest
  variables: 
    REDIS_PORT: 6379
    REDIS_HOST: redis
    REDIS_URL: redis://redis:6379
  script:
    - go mod tidy
    - go build 
    - go test -cover ./... -v -timeout 30m

Memorystore setup:
  needs: [Build & Test]
  extends: .gcloud-job-setup
  script:
    - gcloud services enable redis.googleapis.com
    - export INSTANCE_EXISTS=$(gcloud redis instances list --filter="name:$GCLOUD_REDIS_INSTANCE" --format="value(name)" --region $GCLOUD_COMPUTE_REGION)
    - if [ -z "$INSTANCE_EXISTS" ]; then make create-memorystore ; fi
    - gcloud redis instances describe $GCLOUD_REDIS_INSTANCE --region=$GCLOUD_COMPUTE_REGION

Cloud Run VPC connector setup:
  needs: [Build & Test]
  extends: .gcloud-job-setup
  script:
    - gcloud services enable compute.googleapis.com
    - export VPC_CONNECTOR_EXISTS=$(gcloud compute networks vpc-access connectors list --filter="name:$GCLOUD_CLOUD_RUN_VPC_CONNECTOR" --format="value(name)" --region=$GCLOUD_COMPUTE_REGION)
    - if [ -z "$VPC_CONNECTOR_EXISTS" ]; then make cloud-run-vpc-connector; fi
    - gcloud compute networks vpc-access connectors describe $GCLOUD_CLOUD_RUN_VPC_CONNECTOR --region=$GCLOUD_COMPUTE_REGION

Build:
  stage: Build
  needs: [Memorystore setup, Cloud Run VPC connector setup]
  extends: .gcloud-job
  script:
    - make build

Deploy:
  stage: Deploy
  needs: [Build]
  extends: .gcloud-job
  script:
    - make deploy

Benchmark Shortener:
  needs: [Deploy]
  extends: .benchmark-job
  script:
    - make test-shortener

Benchmark Redirect:
  needs: [Benchmark Shortener]
  extends: .benchmark-job
  script:
    - make test-redirect


Clean All:
  stage: Cleanup
  extends: .gcloud-job
  script:
    - make clean-all
  when: manual

Clean Redis Forwarder:
  stage: Cleanup
  extends: .gcloud-job
  script:
    - make clean-redis-forwarder
  when: manual

Clean Cloud Run VPC Connector:
  stage: Cleanup
  extends: .gcloud-job
  script:
    - make clean-connection
  when: manual

Clean Memorystore:
  stage: Cleanup
  extends: .gcloud-job
  script:
    - make clean-redis
  when: manual

Clean Cloud Run:
  stage: Cleanup
  extends: .gcloud-job
  script:
    - make clean-cloud-run
  when: manual
  
Clean Release Image:
  stage: Cleanup
  extends: .gcloud-job
  script:
    - make clean-image
  when: manual
