package shortener

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestShortLinkGenerator(t *testing.T) {
	initialLink1 := "https://www.mercadolibre.com.ar/"
	shortLink1 := GenerateShortLink(initialLink1)

	initialLink2 := "https://www.mercadolibre.com.br/"
	shortLink2 := GenerateShortLink(initialLink2)

	initialLink3 := "https://www.mercadolibre.cl"
	shortLink3 := GenerateShortLink(initialLink3)

	assert.Equal(t, shortLink1, "eC9WXonT")
	assert.Equal(t, shortLink2, "exQQS3EM")
	assert.Equal(t, shortLink3, "g4Nif5pE")
}
