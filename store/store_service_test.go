package store

import (
	"testing"

	"github.com/lukas-henry/meli-challenge/logger"
	"github.com/stretchr/testify/assert"
)

var (
	initialLink = "https://www.mercadolibre.com.ar/"
	shortURL    = "eC9WXonT"
)

var testStoreService = &StorageService{}

func init() {
	logger.InitializeLogger()
	testStoreService = InitializeStore()
}

func TestRedisEnvs(t *testing.T) {
	assert.NotEmpty(t, redisHost)
}

func TestStoreInit(t *testing.T) {
	assert.True(t, testStoreService.redisClient != nil)
}

func TestInsertionAndRetrieval(t *testing.T) {
	SaveURLMapping(shortURL, initialLink)
	initialURL, notExists := GetInitialURL(shortURL)

	if notExists {
		assert.Equal(t, initialURL, "")
	}

	urlInfo1 := RetrieveURLInfo(shortURL)
	assert.Equal(t, initialLink, urlInfo1.OriginalURL)

	// Test that the access count is incremented
	RetrieveInitialURL(shortURL)
	urlInfo2 := RetrieveURLInfo(shortURL)
	assert.Equal(t, urlInfo1.AccessCount+1, urlInfo2.AccessCount)
	// assert.True(t, urlInfo2.LastAccess.After(urlInfo1.CreatedAt))
	assert.Equal(t, initialLink, urlInfo2.OriginalURL)
}

func TestGetInitialUrl(t *testing.T) {
	SaveURLMapping(shortURL, initialLink)
	initialURL, notExists := GetInitialURL(shortURL)

	if notExists {
		assert.Equal(t, initialURL, "")
	}

	assert.Equal(t, initialLink, initialURL)
}

func TestSaveUrlMapping(t *testing.T) {
	SaveURLMapping(shortURL, initialLink)
	urlInfo := RetrieveURLInfo(shortURL)
	assert.Equal(t, initialLink, urlInfo.OriginalURL)
}

func TestUpdateUrlInfo(t *testing.T) {
	SaveURLMapping(shortURL, initialLink)
	urlInfo1 := RetrieveURLInfo(shortURL)

	UpdateURLInfo(shortURL)
	urlInfo2 := RetrieveURLInfo(shortURL)
	assert.Equal(t, urlInfo1.AccessCount+1, urlInfo2.AccessCount)
	// assert.True(t, urlInfo2.LastAccess.After(urlInfo1.CreatedAt))
	assert.Equal(t, initialLink, urlInfo2.OriginalURL)
}

func TestVars(t *testing.T) {
	assert.Equal(t, originalURLKey, "url")
	assert.Equal(t, createdAtKey, "createdAt:")
	assert.Equal(t, lastAccessKey, "lastAccess:")
	assert.Equal(t, accessCountKey, "accessCount:")
}
