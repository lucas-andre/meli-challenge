package logger

import (
	"log"

	"go.uber.org/zap"
)

var Log *zap.SugaredLogger

func InitializeLogger() {
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatal(err)
	}
	Log = logger.Sugar()
}
