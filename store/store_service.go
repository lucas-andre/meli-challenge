package store

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/go-redis/redis"
	"github.com/lukas-henry/meli-challenge/logger"
)

// StorageService is the interface for the storage service
// that will be used to store the redis client
type StorageService struct {
	redisClient *redis.Client
}

// URLInfo is the struct that will be used to store the information
type URLInfo struct {
	OriginalURL string    `redis:"url" json:"url"`
	CreatedAt   time.Time `redis:"createdAt" json:"createdAt"`
	LastAccess  time.Time `redis:"lastAccess" json:"lastAccess"`
	AccessCount int       `redis:"accessCount" json:"accessCount"`
}

var (
	originalURLKey = "url"
	createdAtKey   = "createdAt:"
	lastAccessKey  = "lastAccess:"
	accessCountKey = "accessCount:"
)

var (
	storeService  = &StorageService{}
	redisHost     = os.Getenv("REDIS_HOST")
	redisPassword = os.Getenv("REDIS_PASSWORD")
)

// InitializeStore initializes the redis client
func InitializeStore() *StorageService {
	logger.Log.Info("Initializing redis client")
	logger.Log.Info("Redis host: ", redisHost)
	redisClient := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:6379", redisHost),
		Password: redisPassword,
		DB:       0,
	})

	_, err := redisClient.Ping().Result()
	if err != nil {
		panic(fmt.Sprintf("Error init Redis: %v", err))
	}

	logger.Log.Info("Redis client initialized")
	storeService.redisClient = redisClient
	return storeService
}

// SaveURLMapping saves the mapping between the short url and the original url
func SaveURLMapping(shortURL string, originalURL string) {
	_, err := storeService.redisClient.Get(shortURL).Result()
	if err != nil {
		if err != redis.Nil {
			return
		}
	}

	now := time.Now().Format(time.RFC3339)
	if _, err := storeService.redisClient.Pipelined(func(p redis.Pipeliner) error {
		storeService.redisClient.HSet(shortURL, originalURLKey, originalURL)
		storeService.redisClient.HSet(shortURL, createdAtKey, now)
		storeService.redisClient.HSet(shortURL, lastAccessKey, now)
		storeService.redisClient.HSet(shortURL, accessCountKey, 0)
		return nil
	}); err != nil {
		panic(fmt.Sprintf("Failed saving key url | Error: %v - shortURL: %s - originalURL: %s\n", err, shortURL, originalURL))
	}
}

// RetrieveInitialURL retrieves the original url from the short url and updates the url info
func RetrieveInitialURL(shortURL string) string {
	result, notExists := GetInitialURL(shortURL)
	if notExists {
		return ""
	}
	UpdateURLInfo(shortURL)
	return result
}

// GetInitialURL retrieves the original url from the short url
func GetInitialURL(shortURL string) (string, bool) {
	result, err := storeService.redisClient.HGet(shortURL, originalURLKey).Result()
	if err != nil {
		if err == redis.Nil {
			return "", true
		}
		panic(fmt.Sprintf("Failed RetrieveInitialUrl url | Error: %v - shortURL: %s\n", err, shortURL))
	}
	return result, false
}

// UpdateURLInfo updates the url info when the url is accessed
func UpdateURLInfo(shortURL string) {
	now := time.Now().Format(time.RFC3339)
	if _, err := storeService.redisClient.Pipelined(func(p redis.Pipeliner) error {
		storeService.redisClient.HSet(shortURL, lastAccessKey, now)
		storeService.redisClient.HIncrBy(shortURL, accessCountKey, 1)
		return nil
	}); err != nil {
		panic(fmt.Sprintf("Failed updating url info | Error: %v - shortURL: %s\n", err, shortURL))
	}
}

// RetrieveURLInfo retrieves the URLInfo struct from the short url
func RetrieveURLInfo(shortURL string) *URLInfo {
	urlInfo := &URLInfo{}
	result, err := storeService.redisClient.HGetAll(shortURL).Result()
	if err != nil {
		if err == redis.Nil {
			return urlInfo
		}
		panic(fmt.Sprintf("Failed RetrieveUrlInfo url | Error: %v - shortURL: %s\n", err, shortURL))
	}

	if len(result) == 0 {
		return urlInfo
	}

	urlInfo.OriginalURL = result[originalURLKey]
	urlInfo.CreatedAt, _ = time.Parse(time.RFC3339, result[createdAtKey])
	urlInfo.LastAccess, _ = time.Parse(time.RFC3339, result[lastAccessKey])
	urlInfo.AccessCount, _ = strconv.Atoi(result[accessCountKey])
	return urlInfo
}

// DeleteURL the short url from the store
func DeleteURL(shortURL string) {
	if err := storeService.redisClient.Del(shortURL).Err(); err != nil {
		panic(fmt.Sprintf("Failed deleting url | Error: %v - shortURL: %s\n", err, shortURL))
	}
}
