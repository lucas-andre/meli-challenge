package main

import (
	"github.com/lukas-henry/meli-challenge/handler"
	"github.com/lukas-henry/meli-challenge/logger"
	"github.com/lukas-henry/meli-challenge/store"

	"github.com/valyala/fasthttp"
)

func main() {
	logger.InitializeLogger()
	store.InitializeStore()
	router := handler.New()

	logger.Log.Info("Starting server on port 8080")
	panic(fasthttp.ListenAndServe(":8080", router.Handler))
}
