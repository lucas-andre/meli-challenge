# Meli Shortener Challenge

This is a simple URL shortener service. It is a REST API written in Go that allows you to create short URLs and redirect them to the original URL.

---

## Contents
- [Meli Shortener Challenge](#meli-shortener-challenge)
  - [Contents](#contents)
  - [Architecture](#architecture)
    - [Gcloud Architecture Implementation](#gcloud-architecture-implementation)
  - [Local Development](#local-development)
    - [Requirements](#requirements)
    - [Debug](#debug)
    - [Local Benchmark](#local-benchmark)
  - [Continuous Integration & Delivery](#continuous-integration--delivery)
  - [API Docs](#api-docs)

## Architecture
The service is composed of two main components: the API and the database. The API is a REST API that allows you to create short URLs, delete short URLs, and redirect to the original URL. The database is a Redis database that stores the short URLs and the original URLs.
### Gcloud Architecture Implementation 
The service is implemented in Google Cloud Serverless architecture. The database is deployed as a Cloud Memorystore Deployment with two replicas. The API is deployed with three replicas to have high availability. The API and the database are deployed in the same region, `southamerica-west1` because we need low latency between the API, the database, and the client. 

![plot](./docs/meli-architecture.png)

---
## Local Development

### Requirements
- Docker
- docker-compose
- make
- Go 1.19

For local development, you can run this.
```bash
make create-local-redis && . ./tools/local-env.sh && go run main.go
```

This will run the Redis database in a docker container and the API in your local machine.
### Debug
For debugging, use VSCode and run the `Launch` configuration. It will attach the debugger to the running process.
### Local Benchmark
For benchmarking, just run
```bash
make test-local-shortener # for run Apache Benchmark on shortener
make test-local-redirect # for run Apache Benchmark on redirect
```
## Continuous Integration & Delivery
The CI is implemented with GitLab CI. This simple CI runs the **linter**, **tests**, **cloud setup**, **builds**, **deploys**, **benchmarks**, and **clean** jobs. The CI is implemented in `.gitlab-ci.yml` file. 
![plot](./docs/meli-CI.png)

---

## API Docs
The documentation is written in Postman Collection format. Therefore, you can import the collection in Postman and run the requests. The documentation is in the `docs` folder.